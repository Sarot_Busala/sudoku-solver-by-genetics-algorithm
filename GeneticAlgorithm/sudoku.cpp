#include <iostream>
#include <vector>
#include <set>
#include "sudoku.h"

Board::Board(){
	 int inigrid[9][9] = { {9,0,6,3,0,0,7,5,0}
				,	{3,1,0,5,2,0,0,0,8}
				,	{5,8,0,7,6,1,0,0,0}
				,	{6,0,8,4,1,3,5,0,0}
				,	{1,7,3,2,9,5,8,0,6}
				,	{4,5,0,0,7,8,0,3,9}
				,	{8,0,5,1,3,2,0,9,0}
				,	{7,0,0,9,0,0,2,0,0}
				,	{2,0,9,8,5,7,3,0,4}
				};
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 9; j++) {
			grid[i][j] = inigrid[i][j];
		}
	}
	calcMoveList();
}

void Board::showBoard() {
	std::cout << "\n";
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 9; j++) {
			std::cout << grid[i][j] << "\t";
		}
		std::cout << "\n";
	}

}

void Board::calcMoveList() {
	std::vector<Move> ret;
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 9; j++) {
			if (grid[i][j] == 0) {
				for (int k = 1; k <= 9; k++) {
					if (checkMoveValid(Move(k, i, j))) {
						ret.push_back(Move(k, i, j));
					}
				}
			}
		}
	}
	moveList = ret;
	moveListN = ret.size();
}

Move Board::getMove(int idx) {
	return moveList[idx];
}

std::vector<Move> Board::getMoveList() {
	return moveList;
}

size_t Board::getMoveListN() {
	return moveListN;
}

bool Board::checkMoveValid(Move move) {
	if (grid[move.r][move.c] != 0) return false;
	for (int i = 0; i < 9; i++) {
		if (grid[move.r][i] == move.num) return false;
		if (grid[i][move.c] == move.num) return false;
	}
	for (int i = 0; i < 9; i++) {
		if (move.r + i < 9 && move.c + i < 9 && grid[move.r + i][move.c + i] != 0) {
			if (grid[move.r + i][move.c + i] == move.num) return false;
		}
		if (move.r + i < 9 && move.c - i >= 0 && grid[move.r + i][move.c - i] != 0) {
			if (grid[move.r + i][move.c - i] == move.num) return false;
		}
		if (move.r - i >= 0 && move.c + i < 9 && grid[move.r - i][move.c + i] != 0) {
			if (grid[move.r - i][move.c + i] == move.num) return false;
		}
		if (move.r - i >= 0 && move.c - i >= 0 && grid[move.r - i][move.c - i] != 0) {
			if (grid[move.r - i][move.c - i] == move.num) return false;
		}
	}
	int inir = move.r / 3 * 3;
	int inic = move.c / 3 * 3;
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			if (grid[inir+i][inic+j] == move.num) {
				return false;
			}
		}
	}
	return true;
}

void Board::doMove(Move move) {
	grid[move.r][move.c] = move.num;
	calcMoveList();
}

int Board::getNum(int r,int c) {
	return grid[r][c];
}
/*
int Board::calculateBoardFitness() {
	if (grid[move.r][move.c] != 0) return false;
	for (int i = 0; i < 9; i++) {
		if (grid[move.r][i] == move.num) return false;
		if (grid[i][move.c] == move.num) return false;
	}
	for (int i = 0; i < 9; i++) {
		if (move.r + i < 9 && move.c + i < 9 && grid[move.r + i][move.c + i] != 0) {
			if (grid[move.r + i][move.c + i] == move.num) return false;
		}
		if (move.r + i < 9 && move.c - i >= 0 && grid[move.r + i][move.c - i] != 0) {
			if (grid[move.r + i][move.c - i] == move.num) return false;
		}
		if (move.r - i >= 0 && move.c + i < 9 && grid[move.r - i][move.c + i] != 0) {
			if (grid[move.r - i][move.c + i] == move.num) return false;
		}
		if (move.r - i >= 0 && move.c - i >= 0 && grid[move.r - i][move.c - i] != 0) {
			if (grid[move.r - i][move.c - i] == move.num) return false;
		}
	}
	for (int inir = 0; inir < 9; inir += 3) {
		for (int inic = 0; inic < 9; inic += 3) {
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					
				}
			}
		}
	}
	return true;
}
*/