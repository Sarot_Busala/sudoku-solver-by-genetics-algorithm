#include <iostream>
#include <random>
#include <vector>
#include "ga.h"

#define TOTAL_GENERATION 100

std::random_device rd;
std::mt19937 gen(rd());
std::uniform_real_distribution<> val_dist(-5.0, 5.0);

int Gene::geneNumber = 0;

Gene::Gene() {
	birthdate = ++Gene::geneNumber;
	board = Board();
	calcFitness();
}

Gene::Gene(Board b) {
	birthdate = ++Gene::geneNumber;
	board = b;
	calcFitness();
}
Gene::~Gene(){}


void Gene::calcFitness() {
	// 4 pts for one correct
	// -1 pts for blank
	int score = 0;
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 9; j++) {
			if (board.getNum(i,j) != 0) {
				score += 4;
			}else {
				score -= 1;
			}
		}
	}
	if (board.getMoveListN() == 0) score = 0;

	fitness = score;
	//std::cout << "FITIFIT" << "\t" << fitness << "\n";
}

void Gene::mutate() {
	size_t moveListN = board.getMoveListN();
	//std::cout << moveListN << "\n";
	if (moveListN == 0) return;
	std::uniform_int_distribution<> selectDist(0, moveListN - 1);
	Move selMove = board.getMove(selectDist(gen));
	moveSeq.push_back(selMove);
	board.doMove(selMove);
	calcFitness();
	//board.showBoard();
	//std::cout << getBirthdate() << "\twalao\n";
}

void Gene::setBirthdate() {
	birthdate = ++Gene::geneNumber;
}

GA::GA() {
	

}

void GA::generateInitialPopulation(int n) {
	N = n;
	for (int i = 1; i <= n; i++) {
		//Gene::generateInitialGene();
		geneVec.push_back(Gene(Board()));
	}
}

void GA::executeGA() {
	generateInitialPopulation(10);
	for (int totalGeneration = 1; true/*totalGeneration <= TOTAL_GENERATION*/; totalGeneration++) {
		generateChildPopulation(10);
		sort(geneVec.begin(), geneVec.end());
		geneVec.resize(N);
		if (((totalGeneration-1)%10000) == 0)showCurrentGeneration();
	}
}

Gene GA::generateChild() {
	std::uniform_int_distribution<> selectDist(0, N-1);
	Gene ret = geneVec[selectDist(gen)];
	ret.mutate();
	ret.setBirthdate();
	//ret.getBoard().showBoard();
	//std::cout << ret.getBirthdate() << "\twalaoaa\n";
	return ret;
}

void GA::generateChildPopulation(int n) {
	for (int i=0;i<n;i++)
		geneVec.push_back(GA::generateChild());
}


void GA::showCurrentGeneration() {
	std::cout << "Birthdate\t\t\tFitness Score\n";
	for (int i = 0; i < N; i++) {
		std::cout << geneVec[i].getBirthdate()  << "\t\t\t" << geneVec[i].getFitness() << "\n";
	}
	std::cout << "\n";
	geneVec[0].getBoard().showBoard();
}

/*
void run_genetic_algorithm() {
	int n, gen_round;
	std::cout << "Input the number of population : ";
	std::cin >> N;
	n = N;
	std::cout << "Input the number of generation to be generated : ";
	std::cin >> gen_round;
	
	generate_initial_population(n);

	for (int _round = 1; _round <= gen_round; _round++) {
		evolve_population();
		if ((_round % 10000) == 0 || _round == 1) {
			std::cout << "ROUND : " << _round << std::endl;
			show_current_generation();
		}
	}


}
*/
